#!/usr/bin/env python
 
import socket
import string
from threading import Thread
from Tkinter import *
import ssl
import random
root = Tk()
root.title("GreenIRC")

goodtext = True

HOST="diluted.hoppy.haus"
PORT=6697
TLS = True
NICK="USER" + str(random.randrange(1,9999))
IDENT="GreeneIRC"
REALNAME="GreeneIRC"
CHANNEL = "#dilute"
readbuffer=""
su=socket.socket( )
if (TLS == True):
    s = ssl.wrap_socket(su)
else:
    s=socket.socket( )
s.connect((HOST, PORT))
s.send("NICK %s\r\n" % NICK)
s.send("USER %s %s bla :%s\r\n" % (IDENT, HOST, REALNAME))
s.send("JOIN " + CHANNEL + "\r\n")
class pinged(Thread):
    def __init__(self):
        Thread.__init__(self)
        self.daemon = True
        self.start()
    def run(self):
        readbuffer = ""
        while 1:
            readbuffer=readbuffer+s.recv(1024)
            temp=string.split(readbuffer, "\n")
            readbuffer=temp.pop( )
            def pretty():
                if (goodtext==True):
                    if(line[1]=='001'):
                        mboxp(feed)
                    if(line[1]=='002'):
                        mboxp(feed)
                    if(line[1]=='003'):
                        mboxp(feed)
                    if(line[1]=='004'):
                        mboxp(feed)
                    if(line[1]=='005'):
                        mboxp(feed)
                    if(line[1]=='251'):
                        mboxp(feed)
                    if(line[1]=='252'):
                        mboxp(feed)
                    if(line[1]=='253'):
                        mboxp(feed)
                    if(line[1]=='254'):
                        mboxp(feed)
                    if(line[1]=='255'):
                        mboxp(feed)
                    if(line[1]=='421'):
                        mboxp("Unknown command: " + line[3])
                    if(line[1]=='372'):
                        mboxp(feed.split(":")[2])
                    if(line[1]=='JOIN'):
                        mboxp(re.match(r"\:(.*)\!", line[0]).group(1) + ' joined ' + line[2])
                    if(line[1]=='PART'):
                        mboxp(re.match(r"\:(.*)\!", line[0]).group(1) + ' left ' + line[2])
                    if(line[1]=='QUIT'):
                        mboxp(re.match(r"\:(.*)\!", line[0]).group(1) + ' quit. (' + feed.split(":")[2] + ')')
                    if(line[1]=='NICK'):
                        mboxp(re.match(r"\:(.*)\!", line[0]).group(1) + ' changed nickname to ' + line[2])
                        s.send('NAMES ' + CHANNEL + '\r\n')
                    if(line[1]=='PRIVMSG'):
                        nk = re.match(r"\:(.*)\!", line[0]).group(1)
                        nknum = 10 - len(nk)
                        nklength = ' ' * nknum + nk
                        mboxp(nklength + '|' + feed.split(":")[2])
                    if(line[1]=='TOPIC'):
                        mboxp(re.match(r"\:(.*)\!", line[0]).group(1) + ' set topic on ' + line[2] + ' to: ' + feed.split(":")[2])
                if(goodtext==False):
                    print line
            for line in temp:
                try:
                    line=string.rstrip(line)
                    feed = line
                    line=string.split(line)
                    pretty()
                    if(line[1]=='332'):
                        title_box.delete(1.0, END)
                        topic = feed.split(":", 2)[2]
                        title_box.insert(END, topic)
                    if(line[1]=='TOPIC'):
                        title_box.delete(1.0, END)
                        topic = feed.split(":", 2)[2]
                        title_box.insert(END, topic)
                    if(line[1]=='PART'):
                        s.send('NAMES ' + CHANNEL + '\r\n')
                    if(line[1]=='JOIN'):
                        s.send('NAMES ' + CHANNEL + '\r\n')
                    if(line[1]=='353'):
                        name_box.delete(1.0, END)
                        names = feed.split(":")[2]
                        splitnames = names.split()
                        splitnames.sort()
                        for name in splitnames:
                            name_box.insert(END, name + '\n')
                    if(line[0]=="PING"):
                        s.send("PONG %s\r\n" % line[1])
                        s.send("NAMES " + CHANNEL + "\r\n")
                except:
                    pass
class StdoutRedirector(object):

    def __init__(self, text_area):
        self.text_area = text_area

    def write(self, str):
        self.text_area.insert(END, str)
        self.text_area.see(END)

def send():
    if (send_box.get().rstrip().split()[0] == '/nick'):
        global NICK
        NICK = send_box.get().rstrip().split()[1]
        s.send("NICK " + NICK + '\r\n')
    else:
        s.send("PRIVMSG " + CHANNEL + ' :' + send_box.get() + '\r\n')
        print NICK + ' > ' + send_box.get()
    send_box.delete(0, 'end')
def entsend(self):
    if (send_box.get().rstrip().split()[0] == '/nick'):
        global NICK
        NICK = send_box.get().rstrip().split()[1]
        s.send("NICK " + NICK + '\r\n')
    if (send_box.get().rstrip().split()[0] == '/quit'):
        s.send("QUIT " + '\r\n')
    elif (send_box.get().rstrip().split()[0][0] == '/'):
        s.send(send_box.get().split('/', 1)[1] + '\r\n')
    else:
        s.send("PRIVMSG " + CHANNEL + ' :' + send_box.get() + '\r\n')
        nknum = 10 - len(NICK)
        nklength = ' ' * nknum + NICK
        print nklength + ' > ' + send_box.get()
    send_box.delete(0, 'end')

def mboxp(putin):
    text_box.insert(END, putin + '\n')
    text_box.see(END)
def txtdef():
    global goodtext
    goodtext = not goodtext
def about_create():
    about_window = Toplevel()
    about_window.title("About")
    Label(about_window, text="GreenIRC", fg = "green", font = "Times").pack()
    Label(about_window, height=11, width=25, wraplength=125, text="\n\n Special thanks to Jeff Greene and Whoa.").pack()

menu_box = Menu(root)
file_menu = Menu(menu_box, tearoff=0)
file_menu.add_command(label="Pretty text", command=txtdef)
file_menu.add_command(label="About", command=about_create)
file_menu.add_command(label="Quit", command=quit)
menu_box.add_cascade(label="File", menu=file_menu)

text_box = Text(root, wrap='word', height=11, width=120)
text_box.grid(column=0, row=1, columnspan=2, sticky='NWSE', padx=5, pady=5)

name_box = Text(root, wrap='word', height=11, width=25)
name_box.grid(column=3, row=1, columnspan=1, sticky='W', padx=5, pady=5)

title_box = Text(root, wrap='word', height=1, width=80)
title_box.grid(column=0, row=0, columnspan=4, sticky='NWSE', padx=5, pady=5)

send_box = Entry(root)
send_box.grid(column=0, row=2, columnspan=1, sticky='NWSE', padx=5, pady=5)

send_button = Button(root, text="Send", command=send)
send_button.grid(column=1, row=2, columnspan=1, sticky='W', padx=5, pady=5)
root.bind('<Return>', entsend)

sys.stdout = StdoutRedirector(text_box)

root.config(menu=menu_box)
pinged()
preface = Toplevel()
preface.title("Preface")
Label(preface, height=10, width=30, wraplength=200, text="This client is built for one thing only: \n\nConnecting to #dilute on my IRC server. If you need more functionality, get a better client, such as HexChat. This however will suffice for connecting to this one channel.").pack()
preface.lift(root)
root.mainloop()